FROM mcr.microsoft.com/mssql-tools as mssql
FROM php:7.0-fpm-alpine

ENV DD_LOGS_INJECTION=true
ENV DD_RUNTIME_METRICS_ENABLED=true
ENV ACCEPT_EULA=Y

# Install prerequisites required for tools and extensions installed later on.
RUN apk add --update bash gnupg libpng-dev libzip-dev su-exec unzip supervisor nginx nginx-mod-http-headers-more

COPY --from=mssql /opt/microsoft/ /opt/microsoft/
COPY --from=mssql /opt/mssql-tools/ /opt/mssql-tools/
COPY --from=mssql /usr/lib/libmsodbcsql-13.so /usr/lib/libmsodbcsql-13.so

RUN set -xe \
    && apk add --no-cache --virtual .persistent-deps \
        freetds \
        unixodbc
RUN apk add --no-cache --virtual .build-deps \
        $PHPIZE_DEPS \
        unixodbc-dev \
        freetds-dev 
RUN docker-php-source extract 
RUN docker-php-ext-install pdo_dblib 
RUN docker-php-source delete 
RUN apk del .build-deps

# Retrieve the script used to install PHP extensions from the source container.
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/bin/install-php-extensions

# Install required PHP extensions and all their prerequisites available via apt.
RUN chmod uga+x /usr/bin/install-php-extensions \
    && sync \
    && install-php-extensions bcmath exif gd intl opcache pcntl redis zip mysqli pdo_mysql pdo_odbc ldap soap imap mcrypt imagick mcrypt pdo_sqlsrv sqlsrv imap

# Downloading composer and marking it as executable.
RUN curl -o /usr/local/bin/composer https://getcomposer.org/composer-stable.phar \
    && chmod +x /usr/local/bin/composer

# RUN apk update && apk add --upgrade php7-pecl-timezonedb
RUN apk add --no-cache --update --virtual buildDeps autoconf build-base
RUN pecl install timezonedb

# Remove HTML folder inside /var/www
RUN rm -rf /var/www/html
RUN mkdir /var/www/public
COPY ./index.php /var/www/public/index.php

# Setting the work directory.
WORKDIR /var/www

# COPY NGINX CONFIG
COPY ./nginx/mime.types /etc/nginx/mime.types
COPY ./nginx/nginx.conf /etc/nginx/nginx.conf

# COPY PHP CONFIGS
COPY ./php/php.ini /usr/local/etc/php/php.ini
COPY ./php/php-fpm.conf /usr/local/etc/php-fpm.conf
COPY ./php/www.conf /usr/local/etc/php-fpm.d/www.conf

# COPY CRONTAB CONFIGS
COPY ./crontabs/cron /etc/cron
RUN chmod 0644 /etc/cron && crontab /etc/cron

# COPY SUPERVISOR CONFIG
COPY ./supervisor/supervisord.conf /etc/supervisord.conf

# COPY SHELLSCRIPT
COPY ./scripts/init.sh /scripts/init.sh
RUN chmod +x /scripts/init.sh

EXPOSE 80